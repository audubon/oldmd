#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get data from http://www.bohlish.com/DFP.cgi?070309
#   DATE:         Sat Mar 10 08:42:00 CST 2007
#
#---------------------------------------------------


use strict;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate/;
use LWP::Simple;
use IO::Scalar;
use Getopt::Std;
$|++;


my %opt=();
getopt('d:',\%opt);

my $date = $opt{d} || UnixDate(ParseDate("today"),"%Y%m%d");
my $dbh   = dbcnx();
my $url   = 'http://www.bohlish.com/DFP.cgi?' . substr($date,2);

print qq^
Date: $date
URL : $url
^;

my @energyfutures = qw(FB CL HU HO NG PN EB RBB);
my @equityfutures = qw(SP DJ YV ND NK NI NN MDH CR GI XG MT LF RL);
my @irfutures     = qw(EL US TY FV TU MB FF ED EM EY);
my @fxfutures     = qw(EU BR JY DX PX CD SF EC AD NE RA MQ BP);
my @grains        = qw(C W S SM BO O);
my @metalfutures  = qw(WA WL WT WN WNN ZN WH WHC WD SI HG PL AL GC);
my @softs         = qw(KC CT SB OJ CC CF QC QW);


my %config=();
@config{@energyfutures} = ('energyfutures') x scalar @energyfutures;
@config{@equityfutures} = ('equityfutures') x scalar @equityfutures;
@config{@irfutures}     = ('irfutures')     x scalar @irfutures;
@config{@fxfutures}     = ('fxfutures')     x scalar @fxfutures;
@config{@grains}        = ('grains')        x scalar @grains;
@config{@metalfutures}  = ('metalfutures')  x scalar @metalfutures;
@config{@softs}         = ('softs')         x scalar @softs;



my $html=get($url);
die "no url return" unless $html;

$html=~/<PRE>(.+)<\/PRE>/is;
my $data=$1;
$data=~s/<[^>]*>//gs;
my $SH = new IO::Scalar \$data;
while( defined(my $line = $SH->getline) ){
    next unless $line=~/^\S/;
    #Name   Date    Open    High    Low Close   Vol Op.Int.
    #@ES07H 070309  140475  141375  139775  140475  494576  1585625 5
    #070309,@ES07H,140475,140475,141375,139775,494576,1585625
    my ($sym,$pdate,$open,$high,$low,$close,$vol,$open_int) = split(/\t/,$line);

    $sym=~/^(\D+)[0-9]+\w/;
    my $con=$1;
    if( exists $config{$con} ){
        my $table=$config{$con};

        if( $table eq 'grains' && ( $con eq 'W' || $con eq 'C' || $con eq 'S' || $con eq 'O')){
            map{ convertgrains($_) }($open, $high, $low, $close);
        }
        if ($con eq 'US') {
            map{convertrates($_)}($open, $high, $low, $close);
        }
        elsif($con eq 'ED' || $con eq 'EM' || $con eq 'FF' || $con eq 'EY' || $con eq 'EL' || $con eq 'HG' || $con eq 'HO'){
            map{$_/=10000}($open, $high, $low, $close);
        }
        elsif ($con eq 'TY' || $con eq 'FV'){
            map{$_/=1000}($open, $high, $low, $close);
        }
        elsif ($con eq 'TU' || $con eq 'CL' || $con eq 'SP' || $con eq 'SI' || $con eq 'EC' || $con eq 'KC'){
            map{$_/=100}($open, $high, $low, $close);
        }
        elsif ($con eq 'GC'){
            map{$_/=10}($open, $high, $low, $close);
        }

        my $insert = qq^
             INSERT into $table
             (date,symbol,close,open,high,low,volume,openinterest)
             values
             (?,?,?,?,?,?,?,?)
             ^;

        my $sth=$dbh->prepare($insert);
        print "$date,$sym,$close,$open,$high,$low,$vol,$open_int\n";
        $sth->execute($date,$sym,$close,$open,$high,$low,$vol,$open_int);
    }
}
$SH->close;

sub convertgrains {
    return if $_[0]=~s/0$/.0/;
    return if $_[0]=~s/2$/.25/;
    return if $_[0]=~s/4$/.5/;
    return if $_[0]=~s/6$/.75/;
}

sub convertrates {
    return if $_[0] == 0;
    $_[0] /= 100;
    $_[0] =~/^(\d+)\.(\d+)$/;
    my $b=$1;
    my $d=$2;
    $d *=10 if length($d)<2;
    $_[0] = $b + $d/32;
}

END{ $dbh->disconnect if defined $dbh }
