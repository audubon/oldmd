-- MySQL dump 8.22
--
-- Host: localhost    Database: marketdata
---------------------------------------------------------
-- Server version 3.23.57
--
-- Table structure for table 'energyfutures'
--

DROP TABLE IF EXISTS energyfutures;
CREATE TABLE energyfutures (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) NOT NULL default '0.000',
  high decimal(6,3) NOT NULL default '0.000',
  low decimal(6,3) NOT NULL default '0.000',
  close decimal(6,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'equityfutures'
--

DROP TABLE IF EXISTS equityfutures;
CREATE TABLE equityfutures (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(7,3) NOT NULL default '0.000',
  high decimal(7,3) NOT NULL default '0.000',
  low decimal(7,3) NOT NULL default '0.000',
  close decimal(7,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'fxfutures'
--

DROP TABLE IF EXISTS fxfutures;
CREATE TABLE fxfutures (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,5) NOT NULL default '0.00000',
  high decimal(6,5) NOT NULL default '0.00000',
  low decimal(6,5) NOT NULL default '0.00000',
  close decimal(6,5) NOT NULL default '0.00000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'fxspot'
--

DROP TABLE IF EXISTS fxspot;
CREATE TABLE fxspot (
  symbol varchar(7) NOT NULL default '0',
  close float NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open float default NULL,
  high float default NULL,
  low float default NULL,
  volume int(9) unsigned default NULL,
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='FX Spot Rates';

--
-- Table structure for table 'grains'
--

DROP TABLE IF EXISTS grains;
CREATE TABLE grains (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) NOT NULL default '0.000',
  high decimal(6,3) NOT NULL default '0.000',
  low decimal(6,3) NOT NULL default '0.000',
  close decimal(6,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'ibdbigmoney'
--

DROP TABLE IF EXISTS ibdbigmoney;
CREATE TABLE ibdbigmoney (
  symbol varchar(5) NOT NULL default '0',
  date timestamp(14) NOT NULL,
  pricechg decimal(5,2) NOT NULL default '0.00',
  volumechg int(11) NOT NULL default '0',
  price decimal(5,2) NOT NULL default '0.00',
  direction varchar(5) NOT NULL default '',
  KEY symbolidx (symbol)
) TYPE=MyISAM COMMENT='investors website';

--
-- Table structure for table 'inflation'
--

DROP TABLE IF EXISTS inflation;
CREATE TABLE inflation (
  symbol varchar(7) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  close float NOT NULL default '0',
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='FRED inflation';

--
-- Table structure for table 'irfutures'
--

DROP TABLE IF EXISTS irfutures;
CREATE TABLE irfutures (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) NOT NULL default '0.000',
  high decimal(6,3) NOT NULL default '0.000',
  low decimal(6,3) NOT NULL default '0.000',
  close decimal(6,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'irrates'
--

DROP TABLE IF EXISTS irrates;
CREATE TABLE irrates (
  symbol varchar(8) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  close float NOT NULL default '0',
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='FRED';

--
-- Table structure for table 'meatfutures'
--

DROP TABLE IF EXISTS meatfutures;
CREATE TABLE meatfutures (
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) unsigned zerofill NOT NULL default '000.000',
  high decimal(6,3) unsigned zerofill NOT NULL default '000.000',
  low decimal(6,3) unsigned zerofill NOT NULL default '000.000',
  close decimal(3,0) unsigned zerofill NOT NULL default '000',
  volume int(7) unsigned zerofill NOT NULL default '0000000',
  openinterest int(7) unsigned zerofill NOT NULL default '0000000',
  symbol varchar(5) NOT NULL default '',
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='meats';

--
-- Table structure for table 'metalfutures'
--

DROP TABLE IF EXISTS metalfutures;
CREATE TABLE metalfutures (
  symbol char(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) NOT NULL default '0.000',
  high decimal(6,3) NOT NULL default '0.000',
  low decimal(6,3) NOT NULL default '0.000',
  close decimal(6,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'metalspot'
--

DROP TABLE IF EXISTS metalspot;
CREATE TABLE metalspot (
  symbol varchar(7) NOT NULL default '0',
  close float NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='Spot metals';

--
-- Table structure for table 'mkt_indexes'
--

DROP TABLE IF EXISTS mkt_indexes;
CREATE TABLE mkt_indexes (
  date date NOT NULL default '0000-00-00',
  close float NOT NULL default '0',
  open float default NULL,
  high float default NULL,
  low float default NULL,
  volume int(9) default NULL,
  symbol varchar(8) NOT NULL default '',
  PRIMARY KEY  (date,symbol)
) TYPE=MyISAM COMMENT='Market Indexes';

--
-- Table structure for table 'softs'
--

DROP TABLE IF EXISTS softs;
CREATE TABLE softs (
  symbol varchar(5) NOT NULL default '0',
  date date NOT NULL default '0000-00-00',
  open decimal(6,3) NOT NULL default '0.000',
  high decimal(6,3) NOT NULL default '0.000',
  low decimal(6,3) NOT NULL default '0.000',
  close decimal(6,3) NOT NULL default '0.000',
  openinterest int(7) unsigned NOT NULL default '0',
  volume int(7) unsigned NOT NULL default '0',
  PRIMARY KEY  (symbol,date)
) TYPE=MyISAM;

--
-- Table structure for table 'stocks'
--

DROP TABLE IF EXISTS stocks;
CREATE TABLE stocks (
  symbol char(5) NOT NULL default '0',
  open decimal(7,2) default '0.00',
  high decimal(7,2) default '0.00',
  low decimal(7,2) default '0.00',
  close decimal(7,2) NOT NULL default '0.00',
  date date NOT NULL default '0000-00-00',
  volume int(7) unsigned default '0',
  PRIMARY KEY  (date,symbol)
) TYPE=ISAM;

--
-- Table structure for table 'volatility'
--

DROP TABLE IF EXISTS volatility;
CREATE TABLE volatility (
  symbol varchar(5) NOT NULL default '0',
  date datetime NOT NULL default '0000-00-00 00:00:00',
  open decimal(5,2) NOT NULL default '0.00',
  high decimal(5,2) NOT NULL default '0.00',
  low decimal(5,2) NOT NULL default '0.00',
  close decimal(5,2) NOT NULL default '0.00',
  PRIMARY KEY  (date,symbol)
) TYPE=ISAM;

