#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get eurex bund,bobl,schatz
#   DATE:         1/21/06 11:12:18 AM
#
#---------------------------------------------------


use strict;
use Date::Manip;
use PRC::DB qw/dbcnx/;
use HTML::TableExtract;
use Getopt::Std;
use LWP::UserAgent;
$|++;

use Data::Dumper;


my %opts=();
getopts('d:',\%opts);

my $date  = $opts{d} || "today";
$date = UnixDate(ParseDate($date),"%Y%m%d");


my %months=(
     Jun => "M"
    ,Sep => "U"
    ,Dec => "Z"
    ,Mar => "H"
);

my %symbols=(
  GL=>"http://www.eurexchange.com/market/statistics/market_statistics/online.html?group=CAP&busdate=${date}&symbol=FGBL"
 ,GM=>"http://www.eurexchange.com/market/statistics/market_statistics/online.html?group=CAP&busdate=${date}&symbol=FGBM"
 ,GS=>"http://www.eurexchange.com/market/statistics/market_statistics/online.html?group=CAP&busdate=${date}&symbol=FGBS"
 ,DAX=>"http://www.eurexchange.com/market/statistics/market_statistics/online.html?group=CAP&busdate=${date}&symbol=FDAX"
);



my $ua = LWP::UserAgent->new;
$ua->agent('Mozilla/5.0');


my $dbh=dbcnx();

my $insert = qq^
INSERT into irfutures
(date,symbol,close,open,high,low,volume,openinterest)
values
(?,?,?,?,?,?,?,?)
^;
my $sth = $dbh->prepare($insert);


my $einsert = qq^
INSERT into equityfutures
(date,symbol,close,open,high,low,volume,openinterest)
values
(?,?,?,?,?,?,?,?)
^;
my $esth = $dbh->prepare($einsert);



foreach my $symbol ( sort keys %symbols ){

    my $url      = $symbols{$symbol};
    my $response = $ua->get($url);

    print "getting $url \n";

    if ($response->is_success) {
        #print $response->content;  # or whatever
    }
    else {
        warn $response->status_line;
        next;
    }
    my $html=$response->content;


    my $te = new HTML::TableExtract( depth => 1, count => 3 );
    $te->parse($html);

    foreach my $ts ($te->table_states) {
    my @x = $ts->rows;
    shift @x;   pop @x;

    #print Dumper(@x);

    foreach my $r(@x) {
        next unless  $r->[5]=~/\S/;
        map{s/\s+|,//g }@$r;
        $r->[0]=~/(\w{3})(\d{2})/;
        my $sym=$symbol.$months{$1} . $2;
        print "$date , $sym , $r->[5] , $r->[1] , $r->[2] ,$r->[3] ,$r->[6] , $r->[7]  \n";
        #date,symbol,close,open,high,low,volume,openinterest
        if ($symbol eq 'DAX'){
        $esth->execute($date , $sym , $r->[5] , $r->[1] , $r->[2] ,$r->[3] ,$r->[6] , $r->[7]);
        }
        else{
            $sth->execute($date , $sym , $r->[5] , $r->[1] , $r->[2] ,$r->[3] ,$r->[6] , $r->[7]);
        }

    }
    }
}


END{ $dbh->disconnect if defined $dbh }
