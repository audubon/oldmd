#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  St. Louis FED FRED database
#   DATE:         3/24/05 7:10:54 PM
#   $Id: fred.pl,v 1.3 2005/11/03 03:20:44 fds Exp $
#
#---------------------------------------------------

use strict;
#use Data::Dumper;
use Date::Manip qw/Date_Cmp/;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use IO::Scalar;
$|++;

my $dbh=dbcnx();


my @fxrates =qw(DEXBZUS DEXCAUS DEXCHUS DEXDNUS DEXHKUS DEXINUS DEXJPUS DEXMAUS DEXMXUS DEXNOUS
        DEXSIUS DEXSFUS DEXKOUS DEXSLUS DEXSDUS DEXSZUS DEXTAUS DEXTHUS DTWEXB  DTWEXM
        DTWEXO  DEXUSAL DEXUSEU DEXUSNZ DEXUSUK DEXVZUS );

my @inflaterates = qw(CPIAUCNS PPIACO PPICEM PPIENG PPIIDC);

my @irrates = qw(DCPN30 DGS1MO DGS1 DGS2 DGS3 DGS5 DGS6 DGS7 DGS10 DGS20 DGS30 DCPN3M DCD90 DTB3
DTB4WK DTB6 WRMORTG DPRIME      DGS3MO   DDISCRT DFF DAAA DBAA DTP10L13 DTP3HA32);



foreach my $sec ( {fxspot => \@fxrates}, {irrates=>\@irrates}, {inflation=>\@inflaterates} ){

    my ($table)   = keys %$sec;
    my ($symbols) = values %$sec;

    # get the max date from the database and then only get the new prices
    my $maxdtsql="select max(date) from $table where symbol=?";
    my $maxsth = $dbh->prepare($maxdtsql);

    my $insert = qq^
    INSERT into $table
    (date,symbol,close)
    values
    (?,?,?)
    ^;
    my $sth = $dbh->prepare($insert);


    foreach my $symbol ( @$symbols ){
        $maxsth->execute($symbol);
        my $maxdate=$maxsth->fetchrow_arrayref;

        my $url="http://research.stlouisfed.org/fred2/series/$symbol/downloaddata/$symbol.txt";
        print 'Getting ', $url, "\n";

        my $text=get($url);
        sleep 3;
        unless (defined $text) {
                print "Couldn't get it $url";
                next;

        }


        my $sh = new IO::Scalar \$text;
        while ( defined(my $x = $sh->getline) ){
            next unless $x=~/^\d{4}/;
            my @data=split ' ',$x;
            map{s/\s+//}@data;
            next if Date_Cmp($data[0],$maxdate->[0]) < 0;
            #print qq^$data[0],$symbol,$data[1] \n^;
            $sth->execute($data[0],$symbol,$data[1]);
        }
        $sh->close;
    }
}



END{ $dbh->disconnect if defined $dbh }
