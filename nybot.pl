#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  NYBOT commodities
#   DATE:         3/9/06 5:24:52 PM
#---------------------------------------------------

use strict;
use Date::Manip qw/UnixDate ParseDate/;
use LWP::Simple;
use Getopt::Std;
use Text::CSV_XS;
use PRC::DB qw/dbcnx/;
use IO::Scalar;

my $usage=q^
-d <date>
-h help
^;

my %opts=();
getopts('d:h',\%opts);
die $usage if $opts{h};


my $dt    = $opts{d} || 'today';
my $date  = UnixDate(ParseDate($dt),'%Y%m%d');
my $year  = substr($date,0,4);
my $month = substr($date,4,2);
my $day   = substr($date,-2);
map{s/^0//}($day,$month);


my $url=qq^http://www.nybot.com/reports/dmrs/download/createFileForAll.asp?commodityType=future&dmrMonth=${month}&dmrDay=${day}&dmrYear=${year}^;
my $content = get($url);
die "Couldn't get $url " unless defined $content;


my $dbh=dbcnx();

my $qry=q^
insert into softs(date,symbol,open,high,low,close,openinterest,volume)
values
(?,?,?,?,?,?,?,?)
^;
my $sth=$dbh->prepare($qry);


my %IMM=(
 '01' =>'F'
,'02' =>'G'
,'03' =>'H'
,'04' =>'J'
,'05' =>'K'
,'06' =>'M'
,'07' =>'N'
,'08' =>'Q'
,'09' =>'U'
,'10' =>'V'
,'11' =>'X'
,'12' =>'Z'
);

my %contracts=(
 CC=>1
,KC=>1
,SB=>1
);


my %lookup=();
my $csv   = Text::CSV_XS->new();



my $SH = new IO::Scalar \$content;

my $head    = <$SH>;
my $hstatus = $csv->parse($head);
my @headers = $csv->fields();


while( my $line=<$SH> ){
    last unless $line=~/\S/;
    $line=~s/\s+//g;

    my $status = $csv->parse($line);
    die "error parsing $!" unless $status;

    my @data = $csv->fields();

    @lookup{@headers}=@data;
    next unless exists $contracts{$lookup{commoditySymbol}};

    my $cm     = substr($lookup{contractMonth},-2);
    my $cy     = substr($lookup{contractMonth},2,2);
    my $symbol = 'N' . $lookup{commoditySymbol} . $IMM{$cm} . $cy;

    print qq^$lookup{tradeDate} , $symbol ,$lookup{dailyOpenPrice1} , $lookup{dailyHigh} ,$lookup{dailyLow} ,$lookup{settlementPrice} ,$lookup{openInterest} , $lookup{tradeVolume} \n^;
    $sth->execute($lookup{tradeDate}
                 ,$symbol
                 ,$lookup{dailyOpenPrice1}
                 ,$lookup{dailyHigh}
                 ,$lookup{dailyLow}
                 ,$lookup{settlementPrice}
                 ,$lookup{openInterest}
                 ,$lookup{tradeVolume}
                 );
}



END{
    $dbh->disconnect if defined $dbh;
}

