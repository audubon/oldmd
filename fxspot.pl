#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  http://research.stlouisfed.org/
#   DATE:         2/27/05 8:53:39 AM
#
#---------------------------------------------------

use strict;
#use Data::Dumper;
use Date::Manip qw/Date_Cmp/;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use IO::Scalar;
$|++;

my $dbh=dbcnx();


my @fxrates =qw(DEXBZUS DEXCAUS DEXCHUS DEXDNUS DEXHKUS DEXINUS DEXJPUS DEXMAUS DEXMXUS DEXNOUS
                DEXSIUS DEXSFUS DEXKOUS DEXSLUS DEXSDUS DEXSZUS DEXTAUS DEXTHUS DTWEXB  DTWEXM
                DTWEXO  DEXUSAL DEXUSEU DEXUSNZ DEXUSUK DEXVZUS );


#XXX get the max date from the database and then only get the new prices
my $maxdtsql='select max(date) from fxspot where symbol=?';
my $maxsth = $dbh->prepare($maxdtsql);

my $insert = qq^
INSERT into fxspot
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);


foreach my $fxrate ( @fxrates ){
        $maxsth->execute($fxrate);
        my $maxdate=$maxsth->fetchrow_arrayref;

    my $url="http://research.stlouisfed.org/fred2/series/$fxrate/downloaddata/$fxrate.txt";
    print 'Getting ', $url, "\n";

    my $text=get($url);
    sleep 3;
    next "Couldn't get it $url" unless defined $text;

    my $sh = new IO::Scalar \$text;
        while ( defined(my $x = $sh->getline) ){
            next unless $x=~/^\d{4}/;
            my @data=split ' ',$x;
            map{s/\s+//}@data;
                next if Date_Cmp($data[0],$maxdate->[0]) < 0;
            #print qq^$data[0],$fxrate,$data[1] \n^;
            $sth->execute($data[0],$fxrate,$data[1]);
    }
    $sh->close;
}

END{ $dbh->disconnect if defined $dbh }
