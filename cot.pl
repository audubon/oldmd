#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  Get/Load commitment of traders report
#   DATE:         10/24/06 9:01:32 PM
#
#---------------------------------------------------

use strict;
use Fatal qw(open close);
use LWP::Simple;
use PRC::DB qw/dbcnx/;
use IO::Scalar;
use Text::CSV_XS;



my $dbh=dbcnx();

my $url = "http://www.cftc.gov/dea/newcot/deafut.txt";
print "getting $url\n";

my $content = get($url);
die "Couldn't get $url " unless defined $content;


my $sql=q^
INSERT INTO COT
(commodity,date,speclong,specshort,commerciallong,commercialshort,openinterest)
VALUES
(? ,? ,? ,? ,? ,? ,?)
^;

my $sth=$dbh->prepare($sql);

my $SH = new IO::Scalar \$content;
my $csv = Text::CSV_XS->new();


while( chomp(my $line=<$SH>) ){
    last unless $line=~/\S/;
    $line=~s/\s+//g;

    my $status = $csv->parse($line);
    die "error parsing $!" unless $status;

    my (
    $MARKETEXCHANGE
    ,$YYMMDD
    ,$YYYYMMDD
    ,$CONTRACTMKTCODE
    ,$MKTCODE
    ,$REGION
    ,$COMMODITY
    ,$OPENINTEREST
    ,$NONCOMMERCIALPOSITIONSLONG
    ,$NONCOMMERCIALPOSITIONSSHORT
    ,$NONCOMMERCIALPOSITIONSSPREADING
    ,$COMMERCIALPOSITIONSLONG
    ,$COMMERCIALPOSITIONSSHORT
    ,$TOTALREPORTABLEPOSITIONSLONG
    ,$TOTALREPORTABLEPOSITIONSSHORT
    ,$NONREPORTABLEPOSITIONSLONG
    ,$NONREPORTABLEPOSITIONSSHORT
    ,$OPENINTERESTOLD
    ,$NONCOMMERCIALPOSITIONSLONGOLD
    ,$NONCOMMERCIALPOSITIONSSHORTOLD
    ,$NONCOMMERCIALPOSITIONSSPREADINGOLD
    ,$COMMERCIALPOSITIONSLONGOLD
    ,$COMMERCIALPOSITIONSSHORTOLD
    ,$TOTALREPORTABLEPOSITIONSLONGOLD
    ,$TOTALREPORTABLEPOSITIONSSHORTOLD
    ,$NONREPORTABLEPOSITIONSLONGOLD
    ,$NONREPORTABLEPOSITIONSSHORTOLD
    ,$OPENINTERESTOTHER
    ,$NONCOMMERCIALPOSITIONSLONGOTHER
    ,$NONCOMMERCIALPOSITIONSSHORTOTHER
    ,$NONCOMMERCIALPOSITIONSSPREADINGOTHER
    ,$COMMERCIALPOSITIONSLONGOTHER
    ,$COMMERCIALPOSITIONSSHORTOTHER
    ,$TOTALREPORTABLEPOSITIONSLONGOTHER
    ,$TOTALREPORTABLEPOSITIONSSHORTOTHER
    ,$NONREPORTABLEPOSITIONSLONGOTHER
    ,$NONREPORTABLEPOSITIONSSHORTOTHER
    ,$CHANGEINOPENINTERESTALL
    ,$CHANGEINNONCOMMERCIALLONGALL
    ,$CHANGEINNONCOMMERCIALSHORTALL
    ,$CHANGEINNONCOMMERCIALSPREADINGALL
    ,$CHANGEINCOMMERCIALLONGALL
    ,$CHANGEINCOMMERCIALSHORTALL
    ,$CHANGEINTOTALREPORTABLELONGALL
    ,$CHANGEINTOTALREPORTABLESHORTALL
    ,$CHANGEINNONREPORTABLELONGALL
    ,$CHANGEINNONREPORTABLESHORTALL
    ,$PCTOFOPENINTERESTOIALL
    ,$PCTOFOINONCOMMERCIALLONGALL
    ,$PCTOFOINONCOMMERCIALSHORTALL
    ,$PCTOFOINONCOMMERCIALSPREADINGALL
    ,$PCTOFOICOMMERCIALLONGALL
    ,$PCTOFOICOMMERCIALSHORTALL
    ,$PCTOFOITOTALREPORTABLELONGALL
    ,$PCTOFOITOTALREPORTABLESHORTALL
    ,$PCTOFOINONREPORTABLELONGALL
    ,$PCTOFOINONREPORTABLESHORTALL
    ,$PCTOFOPENINTERESTOIOLD
    ,$PCTOFOINONCOMMERCIALLONGOLD
    ,$PCTOFOINONCOMMERCIALSHORTOLD
    ,$PCTOFOINONCOMMERCIALSPREADINGOLD
    ,$PCTOFOICOMMERCIALLONGOLD
    ,$PCTOFOICOMMERCIALSHORTOLD
    ,$PCTOFOITOTALREPORTABLELONGOLD
    ,$PCTOFOITOTALREPORTABLESHORTOLD
    ,$PCTOFOINONREPORTABLELONGOLD
    ,$PCTOFOINONREPORTABLESHORTOLD
    ,$PCTOFOPENINTERESTOIOTHER
    ,$PCTOFOINONCOMMERCIALLONGOTHER
    ,$PCTOFOINONCOMMERCIALSHORTOTHER
    ,$PCTOFOINONCOMMERCIALSPREADINGOTHER
    ,$PCTOFOICOMMERCIALLONGOTHER
    ,$PCTOFOICOMMERCIALSHORTOTHER
    ,$PCTOFOITOTALREPORTABLELONGOTHER
    ,$PCTOFOITOTALREPORTABLESHORTOTHER
    ,$PCTOFOINONREPORTABLELONGOTHER
    ,$PCTOFOINONREPORTABLESHORTOTHER
    ,$TRADERSTOTALALL
    ,$TRADERSNONCOMMERCIALLONGALL
    ,$TRADERSNONCOMMERCIALSHORTALL
    ,$TRADERSNONCOMMERCIALSPREADINGALL
    ,$TRADERSCOMMERCIALLONGALL
    ,$TRADERSCOMMERCIALSHORTALL
    ,$TRADERSTOTALREPORTABLELONGALL
    ,$TRADERSTOTALREPORTABLESHORTALL
    ,$TRADERSTOTALOLD
    ,$TRADERSNONCOMMERCIALLONGOLD
    ,$TRADERSNONCOMMERCIALSHORTOLD
    ,$TRADERSNONCOMMERCIALSPREADINGOLD
    ,$TRADERSCOMMERCIALLONGOLD
    ,$TRADERSCOMMERCIALSHORTOLD
    ,$TRADERSTOTALREPORTABLELONGOLD
    ,$TRADERSTOTALREPORTABLESHORTOLD
    ,$TRADERSTOTALOTHER
    ,$TRADERSNONCOMMERCIALLONGOTHER
    ,$TRADERSNONCOMMERCIALSHORTOTHER
    ,$TRADERSNONCOMMERCIALSPREADINGOTHER
    ,$TRADERSCOMMERCIALLONGOTHER
    ,$TRADERSCOMMERCIALSHORTOTHER
    ,$TRADERSTOTALREPORTABLELONGOTHER
    ,$TRADERSTOTALREPORTABLESHORTOTHER
    ,$CONCENTRATIONGROSSLT4TDRLONGALL
    ,$CONCENTRATIONGROSSLT4TDRSHORTALL
    ,$CONCENTRATIONGROSSLT8TDRLONGALL
    ,$CONCENTRATIONGROSSLT8TDRSHORTALL
    ,$CONCENTRATIONNETLT4TDRLONGALL
    ,$CONCENTRATIONNETLT4TDRSHORTALL
    ,$CONCENTRATIONNETLT8TDRLONGALL
    ,$CONCENTRATIONNETLT8TDRSHORTALL
    ,$CONCENTRATIONGROSSLT4TDRLONGOLD
    ,$CONCENTRATIONGROSSLT4TDRSHORTOLD
    ,$CONCENTRATIONGROSSLT8TDRLONGOLD
    ,$CONCENTRATIONGROSSLT8TDRSHORTOLD
    ,$CONCENTRATIONNETLT4TDRLONGOLD
    ,$CONCENTRATIONNETLT4TDRSHORTOLD
    ,$CONCENTRATIONNETLT8TDRLONGOLD
    ,$CONCENTRATIONNETLT8TDRSHORTOLD
    ,$CONCENTRATIONGROSSLT4TDRLONGOTHER
    ,$CONCENTRATIONGROSSLT4TDRSHORTOTHER
    ,$CONCENTRATIONGROSSLT8TDRLONGOTHER
    ,$CONCENTRATIONGROSSLT8TDRSHORTOTHER
    ,$CONCENTRATIONNETLT4TDRLONGOTHER
    ,$CONCENTRATIONNETLT4TDRSHORTOTHER
    ,$CONCENTRATIONNETLT8TDRLONGOTHER
    ,$CONCENTRATIONNETLT8TDRSHORTOTHER
    ,$CONTRACTUNITS
    ,$CFTCCONTRACTMARKETCODEQUOTES
    ,$CFTCMARKETCODEININITIALSQUOTES
    ,$CFTCCOMMODITYCODEQUOTES
    ) = $csv->fields();  #split(/,/,$line);


    $MARKETEXCHANGE=~s/CHICAGOMERCANTILEEXCHANGE/CME/g;
    $MARKETEXCHANGE=~s/CHICAGOBOARDOFTRADE/CBOT/g;
    $MARKETEXCHANGE=~s/NEWYORKBOARDOFTRADE/NYBOT/g;
    $MARKETEXCHANGE=~s/CBOEFUTURESEXCHANGE/CFE/g;
    $MARKETEXCHANGE=~s/NEWYORKMERCANTILEEXCHANGE/NYMEX/g;
    $MARKETEXCHANGE=~s/KANSASCITYBOARDOFTRADE/KCBOT/g;
    $MARKETEXCHANGE=~s/MINNEAPOLISGRAINEXCHANGE/MDE/g;
    $MARKETEXCHANGE=~s/COMMODITYEXCHANGEINC\./COMEX/g;

    print "$MARKETEXCHANGE 20$YYMMDD $OPENINTEREST $NONCOMMERCIALPOSITIONSLONG $NONCOMMERCIALPOSITIONSSHORT $COMMERCIALPOSITIONSLONG $COMMERCIALPOSITIONSSHORT \n";
    #commodity,date,speclong,specshort,commerciallong,commercialshort,openinterest
    $sth->execute($MARKETEXCHANGE, "20$YYMMDD"
          ,$NONCOMMERCIALPOSITIONSLONG ,$NONCOMMERCIALPOSITIONSSHORT
          ,$COMMERCIALPOSITIONSLONG ,$COMMERCIALPOSITIONSSHORT, $OPENINTEREST);
}


END{
    $dbh->disconnect if defined $dbh;
}

