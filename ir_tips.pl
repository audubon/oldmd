#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get real yield curve from treas site
#   DATE:         1/12/06 6:44:28 PM
#   $Id: ir_tips.pl,v 1.1 2006/05/18 04:56:56 sdfsfd Exp $
#
#---------------------------------------------------




use strict;
use LWP::Simple;
use XML::Twig;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_Cmp/;
use Getopt::Std;



my %opts=();
getopt('d:', \%opts);

my $xdate  = $opts{d} || "today";
$xdate = UnixDate(ParseDate($xdate),"%Y%m%d");


#my $url='http://www.treasury.gov/offices/domestic-finance/debt-management/interest-rate/real_yield_historical.xml';
my $url='http://www.treasury.gov/offices/domestic-finance/debt-management/interest-rate/real_yield.xml';


my $html =get($url);
die "Cannot get $url" unless defined $html;



my $dbh=dbcnx();

my $insert = qq^
INSERT into irrates
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);


my $twig=XML::Twig->new(
        twig_roots =>{
            G_NEW_DATE => \&G_NEW_DATE
        }
    );

$twig->parse($html);
$twig->purge;
#$twig->flush;



sub G_NEW_DATE{
    my ($t,$a)=@_;
    print "\n\n";

    my $date = $a->first_child('TIPS_CURVE_DATE')->text;
    $date    = UnixDate(ParseDate($date),'%Y%m%d');

    my $te = $a->first_child('LIST_G_TC_5YEAR');
    my $e  = $te->first_child('G_TC_5YEAR');

    foreach my $k( $e->children() ){
        if ($k->text){
            my $sym  =  $k->name;
            $sym     =~ s/YEAR/YR/g;
            my $rate =  $k->text;
            print $date,' ',$sym, ' ', $rate, "\n";

            $sth->execute($date,$sym,$rate);
        }
    }

    $t->purge;
}


END{
    $dbh->disconnect if defined $dbh;
}

