#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get SPI futures data eod
#   DATE:         5/24/2005  6:34:57 PM
#
#---------------------------------------------------



use Date::Manip qw/UnixDate ParseDate/;
use Getopt::Std;
use HTML::TableExtract;
use LWP::Simple;
use PRC::DB qw/dbcnx/;
use strict;


my %opts=();
getopt('d:', \%opts);


my $dbh = dbcnx();
die "Database connection error " unless defined $dbh;

my $xdt    = $opts{d} || "today";
my $dt       = UnixDate(ParseDate($xdt),"%y%m%d");
my $yyyymmdd = UnixDate(ParseDate($xdt),"%Y%m%d");


my $insert = qq^
INSERT into equityfutures
(date,symbol,close,open,high,low,volume,openinterest)
values
(?,?,?,?,?,?,?,?)
^;
my $sth = $dbh->prepare($insert);


my $irinsert = qq^
INSERT into irfutures
(date,symbol,close,open,high,low,volume,openinterest)
values
(?,?,?,?,?,?,?,?)
^;
my $irsth = $dbh->prepare($irinsert);


my %sym=(
     Jun => "M"
    ,Sep => "U"
    ,Dec => "Z"
    ,Mar => "H"
);


my $url = qq^http://www.sfe.com.au/Content/reports/EODWebMarketSummary${dt}SFD.htm^;
my $html=get($url);
die "Error retrieving $url" unless defined $html;


my $te = new HTML::TableExtract( depth => 0, count => 1 );
$te->parse($html);


### SPI stock index
{

my $found=0;
my $ct=0;

foreach my $ts ($te->table_states) {
    my @x = $ts->rows;
    foreach my $r(@x) {
    next unless $r=~/\S/;
    $found=1 if $r->[0]=~/AP\s+-\s+SPI/;
    if ($found && $r->[0]=~/\w{3}\s\d{4}/) {
        map{s/,//}@$r;
        my ($expiry,$open,$high,$low,$last,$sett,$sett_chg,$op_int,$op_int_chg,$volume)= @$r;

        my $mon = substr($expiry,0,3);
        my $yr=substr($dt,0,2);
        my $symbol = "AP".$sym{$mon}. "$yr";

        print "$yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int \n";
        $sth->execute($yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int);

        last if ++$ct>1;
    }
    }
}

}


###### 3yr bonds

{

my $found=0;
my $ct=0;

foreach my $ts ($te->table_states) {
    my @x = $ts->rows;
    foreach my $r(@x) {
    next unless $r=~/\S/;
    $found=1 if $r->[0]=~/YT\s+-\s+3/;
    if ($found && $r->[0]=~/\w{3}\s\d{4}/) {
        map{s/,//}@$r;
        my ($expiry,$open,$high,$low,$last,$sett,$sett_chg,$op_int,$op_int_chg,$volume)= @$r;

        my $mon = substr($expiry,0,3);
        my $yr=substr($dt,0,2);
        my $symbol = "AYT".$sym{$mon}. "$yr";

        print "$yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int \n";
        $irsth->execute($yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int);

        last if ++$ct>1;
    }
    }
}

}


###### 10yr bonds

{

my $found=0;
my $ct=0;

foreach my $ts ($te->table_states) {
    my @x = $ts->rows;
    foreach my $r(@x) {
    next unless $r=~/\S/;
    $found=1 if $r->[0]=~/XT\s+-\s+10/;
    if ($found && $r->[0]=~/\w{3}\s\d{4}/) {
        map{s/,//}@$r;
        my ($expiry,$open,$high,$low,$last,$sett,$sett_chg,$op_int,$op_int_chg,$volume)= @$r;

        my $mon = substr($expiry,0,3);
        my $yr=substr($dt,0,2);
        my $symbol = "AXT".$sym{$mon}. "$yr";

        print "$yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int \n";
        $irsth->execute($yyyymmdd,$symbol,$sett,$open,$high,$low,$volume,$op_int);

        last if ++$ct>1;
    }
    }
}

}

END{ $dbh->disconnect; }
