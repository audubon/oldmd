#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  futures Data from mrci.com
#   DATE:         6/25/05 11:40:15 AM
#---------------------------------------------------

use strict;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use HTML::TableExtract;
use Date::Manip qw/UnixDate ParseDate/;
use Getopt::Std;
$|++;

my %cmdty_mth=(
     JAN => 'F'
    ,FEB => 'G'
    ,MAR => 'H'
    ,APR => 'J'
    ,MAY => 'K'
    ,JUN => 'M'
    ,JUL => 'N'
    ,AUG => 'Q'
    ,SEP => 'U'
    ,OCT => 'V'
    ,NOV => 'X'
    ,DEC => 'Z'
);

my %opts=();
getopt('d:', \%opts);

my $date  = $opts{d} || "today";
$date = UnixDate(ParseDate($date),"%Y%m%d");
my $year   = substr($date,0,4);
my $xdate = substr($date,2,6);

#http://www.mrci.com/ohlc/ohlc-all.asp
my $url  = "http://www.mrci.com/ohlc/$xdate.asp";
my $html = get($url);
die "Cannot get $url" unless defined $html;


my $dbh=dbcnx();


my $te = new HTML::TableExtract( depth => 1, count => 1   );
#Mth    Date    Open    High    Low     Close   Change  Volume  Open Int    Change

my @data=();
my %zsymbol=(
     'Brent Crude Oil(IPE)'    => [ 'BC','energyfutures' ]
    ,'Gas Oil(IPE)'            => ['GO','energyfutures']
    ,'German Euro-Bund(EUREX)' => ['EB','irfutures']
    ,'German Euro-Bobl(EUREX)' => ['BO','irfutures']
    ,'London Cocoa(LCE)'       => ['LC','softs']
    ,'London Coffee(LCE)'      => ['LK','softs']
    ,'London Sugar(LCE)'       => ['LS','softs']
    ,'Nikkei 225(SIMEX)'       => ['NS','equityfutures']
);


$te->parse($html);
foreach my $ts ($te->table_states) {
    foreach my $row ($ts->rows) {
    next if $row->[0]=~/Tota/;

    if ( $row->[1] eq "" ){
    push @data,$row->[0];
    next;
    }

    map{s/\s+//g}@$row;
    next unless defined $zsymbol{$data[$#data]} ;
    my $mth=$cmdty_mth{ uc(substr($row->[0],0,3))};
    my $yr=substr($row->[0],-2);
    map{s/[+,]//g} ($row->[7] , $row->[8]);

    print qq^$zsymbol{$data[$#data]}[0]$mth$yr,20$row->[1],$row->[2],$row->[3], $row->[4], $row->[5],$row->[7],$row->[8] \n^;


    my $insert = qq^
    INSERT into $zsymbol{$data[$#data]}[1]
    (symbol,date,open,high,low,close,volume,openinterest)
    values
    (?,?,?,?,?,?,?,?)
    ^;
    my $sth = $dbh->prepare($insert);

    $sth->execute(   "$zsymbol{$data[$#data]}[0]$mth$yr"
        ,"20$row->[1]"
        ,$row->[2]
        ,$row->[3]
        ,$row->[4]
        ,$row->[5]
        ,$row->[7]
        ,$row->[8]  );
     }
}


END{ $dbh->disconnect if defined $dbh }
