#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  isada swap rates
#   DATE:         11/1/05 5:49:44 PM
#---------------------------------------------------


use strict;
use PRC::DB qw/dbcnx/;
use Spreadsheet::ParseExcel;
use Date::Manip qw/UnixDate ParseDate DateCalc Date_Cmp/;
use Getopt::Std;
use LWP::Simple;


my $usage =qq^
$0
-f <excel file download> required
     ISDAFIXUSD.xls ISDAFIXEUR-EUIBOR.xls ISDAFIXJPY.xls
     ISDAFIXCHF.xls ISDAFIXGBP.xls ISDAFIXHKD.xls
-d <number days ago to read>
-h usage
^;
my %opts=();
getopts('f:d:h',\%opts) and my $file = $opts{f} or die $usage;

die $usage if $opts{h};
my $days = $opts{d} || 8;

my $url='http://www.isda.org/fix/xls/' . $file; #ISDAFIXUSD.xls';
my $excel='/tmp/' . $file;
getstore($url,$excel) or die "Error retrieving $url";


# set the symbol prefix here ... probably not a good idea to check the
# file name explicitly
my $prefix=undef;
if ($file eq 'ISDAFIXUSD.xls') {
    $prefix = 'S';
}
elsif ($file eq 'ISDAFIXEUR-EUIBOR.xls') {
    $prefix = 'ES';
}
elsif ($file eq 'ISDAFIXJPY.xls') {
    $prefix = 'YS';
}
elsif ($file eq 'ISDAFIXCHF.xls') {
    $prefix = 'SF';
}
elsif ($file eq 'ISDAFIXHKD.xls') {
    $prefix = 'HK';
}
elsif ($file eq 'ISDAFIXGBP.xls') {
    $prefix = 'BP';
}


die "Symbol prefix undefined" unless defined $prefix;


my $daysago=UnixDate(ParseDate(DateCalc("today","+${days} days ago")),"%Y%m%d");
print $daysago,"\n";

my $dbh=dbcnx();
my $qry=q^
INSERT INTO irrates
(date,symbol,close)
values
(?,?,?)
^;
my $sth=$dbh->prepare($qry);


my $oBook = Spreadsheet::ParseExcel::Workbook->Parse($excel);
my($iR, $iC, $oWkS, $oWkC, $date);
my @symbols=();

foreach my $oWkS (@{$oBook->{Worksheet}}) {
    for(my $iR = $oWkS->{MinRow};defined $oWkS->{MaxRow} && $iR <= $oWkS->{MaxRow}; $iR++) {

        for(my $iC = $oWkS->{MinCol};defined $oWkS->{MaxCol} && $iC <= $oWkS->{MaxCol}; $iC++) {

            $oWkC = $oWkS->{Cells}[$iR][$iC];
            if( $iR == $oWkS->{MinRow} ){
                push @symbols,$oWkC->Value if ($oWkC);
            }
            else {
                if( $iC == $oWkS->{MinCol} ){
                    $date = UnixDate(ParseDate($oWkC->Value),"%Y%m%d");
                }
                else {
                    if( $oWkC ){
                        my $r=$oWkC->Value;
                        next unless $r=~/\d+\.\d+/;
                        my $s= $prefix . $symbols[$iC];
                        next unless Date_Cmp($daysago,$date)<0;
                        print "$date $s ", $r, "\n";
                        $sth->execute($date,$s,$r);
                    }
                }
            }
        }
    }
}


END{
    $dbh->disconnect if defined $dbh;
}




