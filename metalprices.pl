#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get pm fix prices from metalprices.com
#   DATE:         Thu Dec  8 21:22:05 CST 2005
#---------------------------------------------------

use strict;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use HTML::TableExtract;
use Date::Manip qw/UnixDate ParseDate/;
$|++;


my $url  = 'http://www.metalprices.com/index.asp';
my $html = get($url);
die "Cannot get $url" unless defined $html;


my $dbh=dbcnx();

my $insert = qq^
INSERT into metalspot
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);


my %metals=(
        ALUMINUM => 'XAL'
        ,ALALLOY => 'XAA'
        ,NASAAC  => 'XST'
        ,COPPER  => 'XHG'
        ,LEAD    => 'XLD'
        ,NICKEL  => 'XNK'
        ,TIN     => 'XTN'
        ,ZINC    => 'XZN'
);


my $dte = new HTML::TableExtract( depth => 6, count =>0 );
die "metalprices date table not defined" unless defined $dte;
$dte->parse($html);
my $date=0;
foreach my $ts ($dte->table_states) {           
    foreach my $row ($ts->rows) {
        chomp(@$row);
                map{s/\s+//g}@$row;

                my $line=join(":",@$row);
                $line=~/(\d{1,2}\/\d{1,2}\/\d{2,4})/;
                $date=$1;

                last if $date;
    }
}

exit unless $date;
$date=UnixDate(ParseDate($date),"%Y%m%d");
print "date ",$date ,"\n";

my $te = new HTML::TableExtract( depth => 6, count =>1 );
die "metalprices table not defined" unless defined $te;
$te->parse($html);
foreach my $ts ($te->table_states) {    
        
    foreach my $row ($ts->rows) {
        chomp(@$row);
                map{s/\s+//g}@$row;
                map{s/,//g}@$row;
                
                next unless $row->[2]=~/\d+/;           
                print qq^$date,$metals{uc $row->[0]},$row->[2]\n^;
                $sth->execute($date,$metals{uc $row->[0]},$row->[2]);
    }
}



END{
        $dbh->disconnect if defined $dbh;
}


