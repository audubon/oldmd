#!/usr/bin/perl

#---------------------------------------------------
#DESCRIPTION:  Parse tradesports.com XML feed
#DATE:         8/16/07 6:36:29 PM
#
#---------------------------------------------------

use strict;
use LWP::Simple;
use XML::Twig;
#use Data::Dumper;


my $url='http://api.tradesports.com/jsp/XML/MarketData/expressBookXML.jsp';

print "getting $url\n\n";
my $content=get($url);
die 'Could not get url' unless defined $content;



#my $file='./sports.xml';


my %sports = (
             NFL => 1
             ,MLB => 1
             ,SOCCER => 1
             ,'College Football' => 1
             );

my $twig=XML::Twig->new(
                       twig_handlers => {
                           'sportsBook/league/game' => \&gamefilter
                       }
                       );
$twig->parse($content);

sub gamefilter  {
    my( $twig, $module)= @_;

#grab the sports we want
    my $league = uc $module->parent()->{'att'}->{'name'};
    next unless exists $sports{$league};


    my $game = $module->{'att'}->{'name'};
    my $home = $module->first_child('teamA')->{'att'}->{'code'};
    my $away = $module->first_child('teamB')->{'att'}->{'code'};

    print "$game : teamA=$home : teamB=$away \n";

    my $win  = $module->first_child('win');
    my @r=$win->children;
    shift @r;
    foreach my $odds( @r ) {
        print 'XWIN : ';
        my $t = $odds->{'att'}->{'for'};
        print $module->first_child($t)->{'att'}->{'code'} , " : ";
        print $odds->first_child('moneyLine')->{'att'}->{'value'}  ," : ";
        print $odds->first_child('digitalOdds')->{'att'}->{'value'}  ," : ";
        print "\n";
    }


    my $spread  = $module->first_child('spread') || $module->first_child('runline');
    if ( defined $spread ) {
        my @sr = $spread->children;
        shift @sr;
        foreach my $odds( @sr ) {
            print 'XSP : ';
            my $t = $odds->{'att'}->{'for'};
            my $strike = $odds->{'att'}->{'strike'};
            print $module->first_child($t)->{'att'}->{'code'} , " : ";
            print $odds->first_child('moneyLine')->{'att'}->{'value'}  ," : ";
            print $odds->first_child('digitalOdds')->{'att'}->{'value'}  ," : ";
            print $strike , " : ";
            print "\n";
        }
    }


    my $total  = $module->first_child('total');
    if ( defined $total ) {
        my @tot = $total->children;
        shift @tot;
        foreach my $odds( @tot ) {
            print 'XOU : ';
            my $t = $odds->{'att'}->{'for'};
            my $totalPoints = $total->{'att'}->{'totalPoints'};
            print $t , " : ";
            print $odds->first_child('moneyLine')->{'att'}->{'value'}  ," : ";
            print $odds->first_child('digitalOdds')->{'att'}->{'value'}  ," : ";
            print $totalPoints , " : ";
            print "\n";
        }
    }

    $twig->purge;
}
