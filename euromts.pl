#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  euro MTS bond indexs
#   DATE:         11/1/05 5:49:44 PM
#   $Id: euromts.pl,v 1.1 2006/11/04 20:43:12 juyd Exp $
#
#---------------------------------------------------


use strict;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate DateCalc Date_Cmp/;
use Getopt::Std;
use LWP::Simple;
use Text::CSV_XS;
use IO::Scalar;


my $usage =qq^
$0
-d <number days ago to read>
-h usage
^;
my %opts=();
getopts('d:h',\%opts)  or die $usage;

die $usage if $opts{h};
my $days = $opts{d} || 3;


my %countries = (
     EMTS   => 'Eurozone_Government_Broad_Close_Hist.csv'
    ,FMTS => 'France_Government_Close_Hist.csv'
    ,GMTS => 'Deutschland_Government_Close_Hist.csv'
    ,IMTS  => 'Italy_Government_Close_Hist.csv'
);

my $baseurl='http://www.euromtsindex.com/index_new/content/emtxg/';

my %lookup=();
my $csv   = Text::CSV_XS->new();


my $daysago=UnixDate(ParseDate(DateCalc("today","+${days} days ago")),"%Y%m%d");
print $daysago,"\n";

my $dbh=dbcnx();

my $qry=q^
INSERT INTO irrates
(date,symbol,close)
values
(?,?,?)
^;

my $sth=$dbh->prepare($qry);



foreach my $country( keys %countries ){
    my $url = $baseurl . $countries{$country};
    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $SH = new IO::Scalar \$content;

    my $head    = <$SH>;
    my $hstatus = $csv->parse($head);
    my @headers = $csv->fields();

    map{s/\s//g ; s/-/_/g ; s/rs/r/g}@headers;

    while( my $line=<$SH> ){
        last unless $line=~/\S/;
        $line=~s/\s+//g;

        my $status = $csv->parse($line);
        die "error parsing $!" unless $status;

        my @data = $csv->fields();

        @lookup{@headers}=@data;

        my $closedate = UnixDate(ParseDate($lookup{'Date'}),'%Y%m%d');
        last if Date_Cmp($daysago,$closedate)>0;

        foreach my $k(keys %lookup) {
            next if $k eq 'Date';
            print $closedate, '  ',$country . $k , '  ',$lookup{$k},"\n";

             $sth->execute(
                  $closedate
                 ,$country . $k
                 ,$lookup{$k}
             );
        }

        %lookup=();
        print "\n";
    }
}


END{
    $dbh->disconnect if defined $dbh;
}
