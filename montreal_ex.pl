#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get Candian 10yr bond futures from montreal exchange
#   DATE:         4/20/06 4:50:42 PM
#---------------------------------------------------

use strict;
use Date::Manip qw/UnixDate ParseDate/;
use LWP::Simple;
use Getopt::Std;
use Text::CSV_XS;
use PRC::DB qw/dbcnx/;
use IO::Scalar;



my %opt=();
getopts('d:',\%opt);
my $d = $opt{d} || "today";

my $date  = UnixDate(ParseDate($d),'%Y%m%d');
my $year  = substr($date,2,2);
my $month = substr($date,4,2);
my $day   = substr($date,-2);

my $y = substr($date,0,4);
my $m = substr($date,4,2);
my $d = substr($date,-2);
my $replace="$y-$m-$d";

my $dbh=dbcnx();

my $qry=q^
insert into irfutures(date,symbol,open,high,low,close,openinterest,volume)
values
(?,?,?,?,?,?,?,?)
^;
my $sth=$dbh->prepare($qry);



my @contracts=qw/CGZ CGB/;

foreach my $contract( @contracts ){

    #my $url=qq^http://www.m-x.ca/donnees_fin_jour_csv.php?lng=en&fannee=${year}&fmois=${month}&fjour=${day}&dannee=${year}&dmois=${month}&djour=${day}&sym_contrat=${contract}^;
    my $url=qq^http://www.m-x.ca/nego_cotes_csv.php?symbol=${contract}&jj=${day}&mm=${month}&aa=${year}&jjF=${day}&mmF=${month}&aaF=${year}^;

    print "Getting $url\n\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    $content=~s/\s//g;
    $content=~s/$replace/\n$replace/g;


    print $content ,"\n";

    my $csv = Text::CSV_XS->new({'sep_char' => ';'});

    my $SH = new IO::Scalar \$content;

    my $head=<$SH>;

    my $hstatus = $csv->parse($head) or die "error parsing $!";
    my @headers = $csv->fields();

    my %lookup=();

    while( my $line=<$SH> ){
        next unless $line=~/\S/;

        my $status = $csv->parse($line) or die "error parsing $!";
        my @data   = $csv->fields();

        @lookup{@headers}=@data;
        next unless  $lookup{SettlementPrice} >0;
        print qq^XXX  $lookup{Date},$lookup{ExtSymbol}, $lookup{OpeningPrice},$lookup{HighPrice},$lookup{LowPrice},$lookup{SettlementPrice},$lookup{OpenInterest},$lookup{Volume} \n^;
         $sth->execute(
          $lookup{Date}
         ,$lookup{ExtSymbol}
         ,$lookup{OpeningPrice}
         ,$lookup{HighPrice}
         ,$lookup{LowPrice}
         ,$lookup{SettlementPrice}
         ,$lookup{OpenInterest}
         ,$lookup{Volume}
          );
    }

}



END{
    $dbh->disconnect if defined $dbh;
}
