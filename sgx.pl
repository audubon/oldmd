#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  Singapore exchange data download
#   DATE:         2/8/06 5:51:04 PM
#
#---------------------------------------------------

use strict;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_PrevWorkDay/;
use Getopt::Std;
use IO::Scalar;
use LWP::Simple;
use WWW::Mechanize;
use Compress::Zlib ;



my $usage =qq^
$0
-d <date> default today
-h usage
^;
my %opts=();
getopts('d:hS',\%opts);
die $usage if $opts{h};

my $xdate  = $opts{d} || "today";
$xdate = UnixDate(ParseDate($xdate),"%Y%m%d");
my $sunday = $opts{S};

if( $sunday ){
        $xdate = Date_PrevWorkDay($xdate,0);
        $xdate = UnixDate(ParseDate($xdate),"%Y%m%d");
}

my $url="http://info.sgx.com/INFOPUBDTStats.nsf/NewVDailyHistPrice/$xdate/";
#my $url="http://info.sgx.com/infopubdtstats.nsf/NewVDailySettlePrice/$xdate/\$File/PRICEFUT$xdate.txt";


my $mech = WWW::Mechanize->new();
$mech->agent_alias( 'Windows IE 6' );
$mech->get( $url );

my $xr=$mech->follow_link( url_regex => qr/Fut/i );
die "Could not follow link" unless defined $xr;
$mech->save_content('/tmp/sgx.zip');

my $fdata = `zcat /tmp/sgx.zip`;



my $dbh=dbcnx();


#---IR futures
my $iqry=q^
    INSERT into irfutures
    (date,symbol,open,high,low,close,volume,openinterest)
    values
    (?,?,?,?,?,?,?,?)
^;
my $isth=$dbh->prepare($iqry);

#---EQUITY futures
my $eqry=q^
    INSERT into equityfutures
    (date,symbol,open,high,low,close,volume,openinterest)
    values
    (?,?,?,?,?,?,?,?)
^;
my $esth=$dbh->prepare($eqry);



my @months=qw(XXX F G H J K M N Q U V X Z);


my $sh = new IO::Scalar \$fdata;

while ( defined(my $line = $sh->getline) ){
    unless (
                $line=~/NK\s+\d{2}/ ||
                $line=~/TW\s+\d{2}/ ||
            $line=~/JB\s+\d{2}/          ){
        next;
    }
    $line=~s/^\s+//;
    $line=~s/,//g;
    $line=~s/\s+/~/g;

    my @data=split('~',$line);
        my $m = substr($data[3],2,2);


    print "$data[0], S$data[1]$months[$data[2]]$m, $data[4],$data[6] ,$data[7],$data[8] ,$data[10],$data[11]\n";
    if( $data[0] eq 'JB'){
        $isth->execute($data[0],"S$data[1]$months[$data[2]]$m"
                ,$data[4]/100,$data[6]/100,$data[7]/100,$data[8]/100,$data[10],$data[11]);
    }
    else {
        $esth->execute($data[0],"S$data[1]$months[$data[2]]$m"
                                   ,$data[4],$data[6],$data[7],$data[8],$data[10],$data[11]);
    }
}

$sh->close;



END{
    $dbh->disconnect if defined $dbh;


        eval{
                unlink "/tmp/sgx.zip";
        }


}
