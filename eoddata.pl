#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  eoddata.com runs Tues through Sat @2:14am
#   DATE:         Mon Mar 21 18:54:21 CST 2005
#
#---------------------------------------------------


use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_PrevWorkDay/;
use WWW::Mechanize;
use IO::Scalar;
use Getopt::Std;
use strict;
$|++;


my $dbh=dbcnx();


my %opts=();
getopt('t:d:', \%opts) and my $type = uc $opts{t} or die "type required";

$type eq 'FOREX' || $type eq 'INDEX' || die "bad type $type";

my $today = UnixDate(ParseDate("today"),"%Y%m%d");
my $dt    = $opts{d} || Date_PrevWorkDay($today,"1","now");
$dt       = UnixDate(ParseDate($dt),"%Y%m%d");

my $table = $type eq 'FOREX' ? 'fxspot' : 'mkt_indexes';
my $insert = qq^
    INSERT into $table
    (date,symbol,close,open,high,low,volume)
    values
    (?,?,?,?,?,?,?)
    ^;
my $sth=$dbh->prepare($insert);



print 'TYPE ', $type , "\n";
print 'Date ', $dt , "\n";



my $mech = WWW::Mechanize->new();
$mech->agent_alias( 'Windows IE 6' );
my $urlsite = 'http://www.eoddata.com';

$mech->get( $urlsite );


sleep int (10 * rand );

sleep int (10 * rand );


my $res =  $mech->submit_form(
     form_name => 'frmLogin'
    ,fields    => {
         Email    => 'perlcs@yahoo.com'
        ,Password => 'nv7y6yi7'
    }
    ,button    => 'Login'
    );
$mech->success or die "post failed: $!";


sleep int (10 * rand );


my $r=$mech->follow_link( url_regex => qr/Download/i );
die "Could not follow link" unless defined $r;


sleep int (10 * rand );


my $fr=$mech->form_name('frmSelect');
my $dc=$mech->select('e', $type);
$mech->submit;


sleep int (10 * rand );

my $xr=$mech->follow_link( url_regex => qr/$dt/i );
die "Could not follow $dt link" unless defined $xr;
my $data = $mech->content;


sleep int (10 * rand );


my $SH = new IO::Scalar \$data;
while( defined(my $line = $SH->getline) ){
    next if $line=~/^</;
    my($sym,$d,$date,$open,$high,$low,$close,$volume)=split(/,/,$line);

    my $pdate = UnixDate(ParseDate($date),"%Y-%m-%d");
    print "$pdate,$sym,$close,$open,$high,$low\n";
    $sth->execute($pdate,$sym,$close,$open,$high,$low,$volume);
}
$SH->close;

