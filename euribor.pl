#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  euribor rates
#   DATE:         1/18/06 5:49:44 PM
#---------------------------------------------------


use strict;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_Cmp/;
use Getopt::Std;
use IO::Scalar;
use LWP::Simple;


my $usage =qq^
$0
-d <first date to read>
-h usage
^;
my %opts=();
getopts('d:h',\%opts);

die $usage if $opts{h};

my $xdate  = $opts{d} || "yesterday";
$xdate = UnixDate(ParseDate($xdate),"%Y%m%d");


my $url  = 'http://www.euribor.org/html/download/euribor_2006.txt';
my $html = get($url);
die "Cannot get $url" unless defined $html;

print "Getting $url\n";

my $dbh=dbcnx();
my $qry=q^
INSERT INTO irrates
(date,symbol,close)
values
(?,?,?)
^;
my $sth=$dbh->prepare($qry);



my @lookup=(
'filler'
,'ERB1W'
,'ERB2W'
,'ERB3W'
,'ERB1M'
,'ERB2M'
,'ERB3M'
,'ERB4M'
,'ERB5M'
,'ERB6M'
,'ERB7M'
,'ERB8M'
,'ERB9M'
, 'ERB10M'
, 'ERB11M'
, 'ERB12M'
);

my $sh = new IO::Scalar \$html;
my @symbols=();

while ( defined(my $x = $sh->getline) ){
    next unless $x=~/\S/;
    next if $x=~/^#/;

    unless (scalar @symbols){
                @symbols = (1); #split(/\t/,$x);
                #map{s/\s+//g}@symbols;
                next;
    }

    $x=~s!,!\.!g;
    my @data = split(/\t/,$x);

        print $data[0] ."\n";
    map{s/\s+//g}@data;

    my $ndate=UnixDate(ParseDate($data[0]),"%Y%m%d");
        print 'DAtE '. $data[0] ."\n";
        print $ndate . "  ". $x ,"\n";

    last if Date_Cmp($ndate,$xdate) < 0;



    for(my $i=1;$i<=$#data;$i++) {
                print "$data[0],$lookup[$i],$data[$i] \n";
                $sth->execute($ndate,$lookup[$i],$data[$i]);
    }
    print "\n";

}
$sh->close;



END{
    $dbh->disconnect if defined $dbh;
}
