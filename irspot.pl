#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  http://www.econstats.com
#   DATE:         3/26/05 10:44:40 AM
#---------------------------------------------------

use strict;
#use Data::Dumper;
use Date::Manip qw/UnixDate ParseDate Date_Cmp/;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use IO::Scalar;
$|++;

my $dbh=dbcnx();

# get the max date from the database and then only get the new prices
my $maxdtsql="select max(date) from irrates where symbol=?";
my $maxsth = $dbh->prepare($maxdtsql);

my $insert = qq^
INSERT into irrates
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);


#------------------------------------------
# TREASURIES
#------------------------------------------
{
    my $url='http://www.econstats.com/r/r__d1.csv';

    print $url , "\n";
    my $text=get($url);

    # compare with max(date) from table
    $maxsth->execute('trsy3m');
    my $maxdate=$maxsth->fetchrow_arrayref;


    my $sh = new IO::Scalar \$text;
    while ( defined(my $line = $sh->getline) ){
    next unless $line=~/^\d{2}-\w{3}-\d{4}/;
    my ($date,$day,$trsy1m,$trsy3m,$trsy6m,$trsy1y,$trsy2y,$trsy3y,$trsy5y,$trsy7y,$trsy10y,$trsy20y,$trsy30y)=split(',',$line);
    $date = UnixDate(ParseDate($date),"%Y%m%d");

    last if Date_Cmp($date,$maxdate->[0]) < 0;

    print $line ,"\n";
    $sth->execute($date,'trsy1m'   ,$trsy1m ) if $trsy1m  >0;
    $sth->execute($date,'trsy3m'   ,$trsy3m ) if $trsy3m  >0;
    $sth->execute($date,'trsy6m'   ,$trsy6m ) if $trsy6m  >0;
    $sth->execute($date,'trsy1y'   ,$trsy1y ) if $trsy1y  >0;
    $sth->execute($date,'trsy2y'   ,$trsy2y ) if $trsy2y  >0;
    $sth->execute($date,'trsy3y'   ,$trsy3y ) if $trsy3y  >0;
    $sth->execute($date,'trsy5y'   ,$trsy5y ) if $trsy5y  >0;
    $sth->execute($date,'trsy7y'   ,$trsy7y ) if $trsy7y  >0;
    $sth->execute($date,'trsy10y'  ,$trsy10y) if $trsy10y >0;
    $sth->execute($date,'trsy20y'  ,$trsy20y) if $trsy20y >0;
    $sth->execute($date,'trsy30y'  ,$trsy30y) if $trsy30y >0;
    }
    $sh->close;
}

#------------------------------------------
#TIPS
#------------------------------------------
{
    my $url='http://www.econstats.com/r/r__d5.csv';

    print $url , "\n";
    my $text=get($url);

    # compare with max(date) from table
    $maxsth->execute('tip5yr');
    my $maxdate=$maxsth->fetchrow_arrayref;


    my $sh = new IO::Scalar \$text;
    while ( defined(my $line = $sh->getline) ){
    next unless $line=~/^\d{2}-\w{3}-\d{4}/;
    my ($date,$day,$tip5yr,$tip7yr,$tip10yr,$tip20yr)=split(',',$line);
    $date = UnixDate(ParseDate($date),"%Y%m%d");

    last if Date_Cmp($date,$maxdate->[0]) < 0;

    print $line ,"\n";
    $sth->execute($date,'tip5yr'  ,$tip5yr)  if $tip5yr  >0;
    $sth->execute($date,'tip7yr'  ,$tip7yr)  if $tip7yr  >0;
    $sth->execute($date,'tip10yr' ,$tip10yr) if $tip10yr >0;
    $sth->execute($date,'tip20yr' ,$tip20yr) if $tip20yr >0;
    }
    $sh->close;
}

#------------------------------------------
# SWAPS
#------------------------------------------
{
    my $url='http://www.econstats.com/r/r__d6.csv';

    print $url , "\n";
    my $text=get($url);

    # compare with max(date) from table
    $maxsth->execute('swap1yr');
    my $maxdate=$maxsth->fetchrow_arrayref;


    my $sh = new IO::Scalar \$text;
    while ( defined(my $line = $sh->getline) ){
    next unless $line=~/^\d{2}-\w{3}-\d{4}/;
    my ($date,$day,$swap1yr,$swap2yr,$swap3yr,$swap4yr,$swap5yr,$swap7yr,$swap10yr,$swap30yr )=split(',',$line);
    $date = UnixDate(ParseDate($date),"%Y%m%d");

    last if Date_Cmp($date,$maxdate->[0]) < 0;

    print                          $line      ,"\n";
    $sth->execute($date,'swap1yr'  ,$swap1yr  )      if $swap1yr  >0;
    $sth->execute($date,'swap2yr'  ,$swap2yr  )      if $swap2yr  >0;
    $sth->execute($date,'swap3yr'  ,$swap3yr  )      if $swap3yr  >0;
    $sth->execute($date,'swap4yr'  ,$swap4yr  )      if $swap4yr  >0;
    $sth->execute($date,'swap5yr'  ,$swap5yr  )      if $swap5yr  >0;
    $sth->execute($date,'swap7yr'  ,$swap7yr  )      if $swap7yr  >0;
    $sth->execute($date,'swap10yr' ,$swap10yr )      if $swap10yr >0;
    $sth->execute($date,'swap30yr' ,$swap30yr )      if $swap30yr >0;
    }
    $sh->close;
}

#------------------------------------------
# Weekly Mortgage
#------------------------------------------
{
    my $url='http://www.econstats.com/r/r_aw9.csv';

    print $url , "\n";
    my $text=get($url);

    # compare with max(date) from table
    $maxsth->execute('arm1yr');
    my $maxdate=$maxsth->fetchrow_arrayref;


    my $sh = new IO::Scalar \$text;
    while ( defined(my $line = $sh->getline) ){
    next unless $line=~/^\d{8}/;
    my    ($date,$day,$arm1yr,$armpts1yr,$armmrg1yr,$mort15yr,$mortpts15yr,$mort30yr )=
    unpack("A8   A6   A11     A13        A12        A11       A13          A11",$line);
    #20050218,5277,     4.1500,     0.8000,           ,     5.1400,     0.7000,     5.6200,     0.7000,

    print "$date $arm1yr $mort15yr $mort30yr \n";

    last if Date_Cmp($date,$maxdate->[0]) < 0;


    $sth->execute($date,'arm1yr'   ,$arm1yr   ) if $arm1yr   >0;
    $sth->execute($date,'mort15yr' ,$mort15yr ) if $mort15yr >0;
    $sth->execute($date,'mort30yr' ,$mort30yr ) if $mort30yr >0;

    }
    $sh->close;
}


END{ $dbh->disconnect if defined $dbh }


