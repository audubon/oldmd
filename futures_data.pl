#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  Get eod data from www.crbtrader.com
#   DATE:         2/27/05 8:53:39 AM
#   $Id: futures_data.pl,v 1.4 2005/03/30 14:40:14 jnhg Exp $
#
#---------------------------------------------------

use strict;
#use Data::Dumper;
use Date::Manip;
use PRC::DB qw/dbcnx/;
use HTML::TableExtract;
use LWP::Simple;
$|++;

my $dbh=dbcnx();


my %config =(
      energyfutures => [ qw(CL HU HO NG PN)             ]
     ,equityfutures => [ qw(SP DJ YV ND NK MDH CR GI)   ]
     ,irfutures     => [ qw(US TY FV TU MB FF ED EM EY) ]
     ,fxfutures     => [ qw(DX SY GB EJ RZ JY CD SF EC AD NE RA MQ BP) ]
     ,grains        => [ qw(C W S SM BO O)              ]
     ,metalfutures  => [ qw(SI HG PL AL GC)             ]
     ,softs         => [ qw(KC CT SB OJ CC LB)          ]
     ,meatfutures   => [ qw(LH PB FC LC DA DB)]
);



foreach my $table (keys %config) {
    my $insert = qq^
    INSERT into $table
    (date,symbol,close,open,high,low,volume,openinterest)
    values
    (?,?,?,?,?,?,?,?)
    ^;
    my $sth = $dbh->prepare($insert);


    foreach my $symbol ( @{$config{$table}} ){
    my $url="http://sites3.barchart.com/pl/crb/quote.htx?sym=$symbol&mode=d";
    print 'Getting ', $url, "\n";
    my $html=get($url);
    sleep 3;
    next "Couldn't get it $url" unless defined $html;

    #remove the javascript stuff
    $html=~s/document\.write\(\'//g;
    $html=~s/\);//g;

    my $te = new HTML::TableExtract( depth => 0, count => 0 );
    $te->parse($html);

    foreach my $ts ($te->table_states) {
        my @x        = $ts->rows;
        my $date_ref = shift @x;
        my ($date)   = ($date_ref->[0]=~/Daily\sFutures\s-\s+(.+?)\s{4,}/isx);
        $date        = UnixDate(ParseDate($date),"%Y-%m-%d");
        shift @x;

        foreach my $row ( @x ){
        map{s/[^0-9\.-]//g}@$row[1..7];
        my ($contract)=($row->[0]=~/.+\((.+)\)/);

        #reprice the 32nds
        if ( $table eq 'irfutures' && $row->[1]=~/-/ ){
            reprice($row,32);
        }
        #reprice the 8ths
        elsif ( $table eq 'grains' && $row->[1]=~/-/ ){
            reprice($row,8);
        }

        print qq^$date,$contract,$row->[1],$row->[3],$row->[4],$row->[5],$row->[6],$row->[7]\n^;
        $sth->execute($date,$contract,$row->[1],$row->[3],$row->[4],$row->[5],$row->[6],$row->[7]);
        }
    }
    }
}

sub reprice {
    my ($row,$base)  = @_;
    for ( @$row[1,3,4,5] ){
        my @gr=split('-');
        my $clean=$gr[0] . '.' . $gr[1]/$base;
        $clean=~s/\.0//;
        $_=$clean;
    }
}

END{ $dbh->disconnect if defined $dbh }
