#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  Get commodity data from LIFFE
#   DATE:         10/26/05 5:58:08 PM
#---------------------------------------------------


use strict;
#use warnings;
use LWP::Simple;
use PRC::DB qw/dbcnx/;
use IO::Scalar;
use Getopt::Std;
use Date::Manip qw/UnixDate ParseDate/;

my %com = (
          JAN => 'F'
         ,FEB => 'G'
         ,MAR => 'H'
         ,APR => 'J'
         ,MAY => 'K'
         ,JUN => 'M'
         ,JUL => 'N'
         ,AUG => 'Q'
         ,SEP => 'U'
         ,OCT => 'V'
         ,NOV => 'X'
         ,DEC => 'Z'
          );

my %opts=();
getopts('d:cpfbar',\%opts);

my $indate  = $opts{d} || "today";
my $getdate = UnixDate(ParseDate($indate),"%y%m%d");

my $paris=1;
my $com  =1;
my $ftse =1;
my $bel  =1;
my $aex  =1;
my $rates=1;

if( $opts{c} || $opts{f} || $opts{p} || $opts{a} || $opts{b} || $opts{r} ){
    $paris = $opts{p} || 0;
    $com   = $opts{c} || 0;
    $ftse  = $opts{f} || 0;
    $bel   = $opts{b} || 0;
    $aex   = $opts{a} || 0;
    $rates = $opts{r} || 0;
}



my $dbh=dbcnx();


if( $com ){
    my $url = "http://www.liffe.com/data/ds${getdate}xf.csv";
    print "getting $url\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $qry=q^
    insert into softs(date,symbol,open,high,low,close,openinterest,volume)
    values
    (?,?,?,?,?,?,?,?)
    ^;

    my $sth=$dbh->prepare($qry);

    my $SH = new IO::Scalar \$content;

    while( chomp(my $line=<$SH>) ){
        $line=~s/\s//g;
        my ($date,$sym,$exp,$volume,$oi,$open,$high,$low,$close)=
          (split(',',$line))[0,1,2,6,7,8,11,12,13];

        my $dt=substr($exp,0,3);
        $exp=~s/$dt/$com{$dt}/;
        $date=UnixDate(ParseDate($date),"%Y%m%d");

        print "$date,X$sym$exp,$volume,$oi,$open,$high,$low,$close";
        print "\n";
        $sth->execute($date,"X${sym}${exp}",$open,$high,$low,$close,$oi,$volume);
    }

}

#cac40
if( $paris ){
    my $url = "http://www.liffe.com/data/p_ds${getdate}if.csv";
    print "getting $url\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $qry=q^
    insert into equityfutures(date,symbol,open,high,low,close,openinterest,volume)
    values
    (?,?,?,?,?,?,?,?)
    ^;

    my $sth=$dbh->prepare($qry);

    my $SH = new IO::Scalar \$content;

    while( chomp(my $line=<$SH>) ){
        $line=~s/\s//g;
        my ($date,$sym,$exp,$volume,$oi,$open,$high,$low,$close)=
          (split(',',$line))[0,1,2,6,7,8,11,12,13];

        my $dt=substr($exp,0,3);
        $exp=~s/$dt/$com{$dt}/;
        $sym=~s/E$//;

        $date=UnixDate(ParseDate($date),"%Y%m%d");

        print "$date,$sym$exp,$volume,$oi,$open,$high,$low,$close \n";

        $sth->execute($date,"${sym}${exp}",$open,$high,$low,$close,$oi,$volume);
    }
}

#brussels index
if( $bel ){
    my $url = "http://www.liffe.com/data/b_ds${getdate}if.csv";
    print "getting $url\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $qry=q^
    insert into equityfutures(date,symbol,open,high,low,close,openinterest,volume)
    values
    (?,?,?,?,?,?,?,?)
    ^;

    my $sth=$dbh->prepare($qry);

    my $SH = new IO::Scalar \$content;

    while( chomp(my $line=<$SH>) ){
        $line=~s/\s//g;
        my ($date,$sym,$exp,$volume,$oi,$open,$high,$low,$close)=
          (split(',',$line))[0,1,2,6,7,8,11,12,13];

        next unless $sym eq 'BXF';
        my $dt=substr($exp,0,3);
        $exp=~s/$dt/$com{$dt}/;

        $date=UnixDate(ParseDate($date),"%Y%m%d");

        print "$date,$sym$exp,$volume,$oi,$open,$high,$low,$close \n";

        $sth->execute($date,"${sym}${exp}",$open,$high,$low,$close,$oi,$volume);
    }
}

#ftse100  index
if( $ftse ){
    my $url = "http://www.liffe.com/data/ds${getdate}ff.csv";
    print "getting $url\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $qry=q^
    insert into equityfutures(date,symbol,open,high,low,close,openinterest,volume)
    values
    (?,?,?,?,?,?,?,?)
    ^;

    my $sth=$dbh->prepare($qry);

    my $SH = new IO::Scalar \$content;

    while( chomp(my $line=<$SH>) ){
        $line=~s/\s//g;
        my ($date,$sym,$exp,$volume,$oi,$open,$high,$low,$close)=
          (split(',',$line))[0,1,2,6,7,8,11,12,13];

        next unless $sym eq 'Z';
        my $dt=substr($exp,0,3);
        $exp=~s/$dt/$com{$dt}/;
        $sym=~s/Z/FTS/;

        $date=UnixDate(ParseDate($date),"%Y%m%d");

        print "$date,$sym$exp,$volume,$oi,$open,$high,$low,$close\n";
        $sth->execute($date,"${sym}${exp}",$open,$high,$low,$close,$oi,$volume);
    }
}


#ir rates
if( $rates ){
    my $url = "http://www.liffe.com/data/ds${getdate}ff.csv";
    print "getting $url\n";

    my $content = get($url);
    die "Couldn't get $url " unless defined $content;

    my $qry=q^
    insert into irfutures(date,symbol,open,high,low,close,openinterest,volume)
    values
    (?,?,?,?,?,?,?,?)
    ^;

    my $sth=$dbh->prepare($qry);

    my $SH = new IO::Scalar \$content;

    while( chomp(my $line=<$SH>) ){
        $line=~s/\s//g;
        my ($date,$sym,$exp,$volume,$oi,$open,$high,$low,$close)=
          (split(',',$line))[0,1,2,6,7,8,11,12,13];

        next unless ($sym eq 'R' || $sym eq 'I' || $sym eq 'L' || $sym eq 'S');
        my $dt=substr($exp,0,3);
        $exp=~s/$dt/$com{$dt}/;
        $sym = 'EN' . $sym;

        $date=UnixDate(ParseDate($date),"%Y%m%d");

        print "$date,$sym$exp,$volume,$oi,$open,$high,$low,$close\n";
        $sth->execute($date,"${sym}${exp}",$open,$high,$low,$close,$oi,$volume);
    }
}


END{
    $dbh->disconnect if defined $dbh;
}

