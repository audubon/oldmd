#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get data from www.spotmarketplace.com
#   DATE:         3/11/2005  1:04:56 PM
#   $Id: hist_futures.pl,v 1.4 2005/09/24 14:00:49 htyg Exp $
#
#---------------------------------------------------


use strict;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_NextWorkDay Date_PrevWorkDay/;
use LWP::Simple;
use IO::Scalar;
use Getopt::Std;
$|++;


my %opt=();
getopt('s:e:',\%opt);


my $today = $opt{e} || UnixDate(ParseDate("today"),"%Y%m%d");
my $start = $opt{s} || UnixDate(ParseDate(Date_PrevWorkDay($today,"2","now")),"%Y%m%d");
my $dbh   = dbcnx();


print qq^
Start: $start
End:   $today
^;

my @energyfutures = qw(FB CL HU HO NG PN EB);
my @equityfutures = qw(SP DJ YV ND NK NI NN MDH CR GI XG MT LF RL);
my @irfutures     = qw(EL US TY FV TU MB FF ED EM EY);
my @fxfutures     = qw(EU BR JY DX PX CD SF EC AD NE RA MQ BP);
my @grains        = qw(C W S SM BO O);
my @metalfutures  = qw(WA WL WT WN WNN ZN WH WHC WD SI HG PL AL GC);
my @softs         = qw(KC CT SB OJ CC CF LB QC QW);
my @meatfutures   = qw(PB LH LC FC);


my %config=();
@config{@energyfutures} = ('energyfutures') x scalar @energyfutures;
@config{@equityfutures} = ('equityfutures') x scalar @equityfutures;
@config{@irfutures}     = ('irfutures')     x scalar @irfutures;
@config{@fxfutures}     = ('fxfutures')     x scalar @fxfutures;
@config{@grains}        = ('grains')        x scalar @grains;
@config{@metalfutures}  = ('metalfutures')  x scalar @metalfutures;
@config{@softs}         = ('softs')         x scalar @softs;
@config{@meatfutures}   = ('meatfutures')  x scalar @meatfutures;



for (my $date=$start;UnixDate(Date_NextWorkDay($date,"1"),"%Y%m%d") <= $today; $date=UnixDate(Date_NextWorkDay($date,"1"),"%Y%m%d") ){
    my $fdate = substr($date,2);

    my $url="http://www.spotmarketplace.com/futures/prices/dl$fdate.htm";
    my $html=get($url);
    next unless $html;

    sleep 1;

    $html=~/<PRE>(.+)<\/PRE>/is;
    my $data=$1;
    $data=~s/<[^>]*>//gs;

    my $SH = new IO::Scalar \$data;
    while( defined(my $line = $SH->getline) ){
    next unless $line=~/^.\w+/;
    my ($sym,$pdate,$open,$high,$low,$close,$chg,$open_int,$prev_vol) =
    unpack("A7 A13 A10 A10 A10 A10 A10 A9 A10",$line);
    map{s/[\s-]//g}($sym,$pdate,$open,$high,$low,$close,$chg,$open_int,$prev_vol);
    $sym=~/(\w+)(\d|\$)/;
    my $table=$config{$1};
    if (defined $table) {
    my $insert = qq^
        INSERT into $table
        (date,symbol,close,open,high,low,volume,openinterest)
        values
        (?,?,?,?,?,?,?,?)
        ^;
    my $sth=$dbh->prepare_cached($insert);
    unless ($pdate=~/\d{2}\/\d{2}\/\d{4}/){
    warn "$pdate errror \n";
    next;
    }

    $pdate = UnixDate(ParseDate($pdate),"%Y-%m-%d");
    print "$date,$pdate,$sym,$table\n$pdate,$sym,$close,$open,$high,$low,$prev_vol,$open_int\n";
    $sth->execute($pdate,$sym,$close,$open,$high,$low,$prev_vol,$open_int);
    }
    }
    $SH->close;
}
