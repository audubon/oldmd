#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  get yield curve from treas site
#   DATE:         1/12/06 6:44:28 PM
#
#---------------------------------------------------

use strict;
use LWP::Simple;
use XML::Simple;
use PRC::DB qw/dbcnx/;
use Date::Manip qw/UnixDate ParseDate Date_Cmp/;
use Getopt::Std;



my %opts=();
getopt('d:', \%opts);

my $xdate  = $opts{d} || "yesterday";
$xdate = UnixDate(ParseDate($xdate),"%Y%m%d");


my $url='http://www.treas.gov/offices/domestic-finance/debt-management/interest-rate/yield.xml';
#my $url='http://www.treas.gov/offices/domestic-finance/debt-management/interest-rate/yield_historical_1998.xml';

my $html = get($url);
die "Cannot get $url" unless defined $html;

my $r=XMLin($html);
die "XML parse error $!" unless defined $r;


my $dbh=dbcnx();


my %duration=(
 US1M => 'BC_1MONTH'
,US3M => 'BC_3MONTH'
,US6M => 'BC_6MONTH'
,US1Y => 'BC_1YEAR'
,US2Y => 'BC_2YEAR'
,US3Y => 'BC_3YEAR'
,US5Y => 'BC_5YEAR'
,US7Y => 'BC_7YEAR'
,US10Y => 'BC_10YEAR'
,US20Y => 'BC_20YEAR'
);


my $insert = qq^
INSERT into irrates
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);


#--
# for parsing historical files
#--
# foreach my $f( @{$r->{LIST_G_BID_CURVE_DATE}{G_BID_CURVE_DATE}} ){
#     my $date=UnixDate(ParseDate($f->{BID_CURVE_DATE}),"%Y%m%d");
#
#     foreach my $sym(keys %duration) {
#         next if ref $f->{LIST_G_BC_30YEAR}{G_BC_30YEAR}{$duration{$sym}};
#         print $date ," $sym ", $f->{LIST_G_BC_30YEAR}{G_BC_30YEAR}{$duration{$sym}} ,"\n";
#         $sth->execute($date,$sym,$f->{LIST_G_BC_30YEAR}{G_BC_30YEAR}{$duration{$sym}});
#     }
# }



my @d=();
if (ref $r->{LIST_G_WEEK_OF_MONTH}{G_WEEK_OF_MONTH} eq 'ARRAY'){
    @d=@{$r->{LIST_G_WEEK_OF_MONTH}{G_WEEK_OF_MONTH}};
}
else {
    @d=$r->{LIST_G_WEEK_OF_MONTH}{G_WEEK_OF_MONTH};
}




foreach my $f(@d) {

    if( ref $f->{LIST_G_NEW_DATE}{G_NEW_DATE} eq 'ARRAY' ){
        foreach my $day( @{$f->{LIST_G_NEW_DATE}{G_NEW_DATE}} ){
            my $date=UnixDate(ParseDate($day->{BID_CURVE_DATE}),"%Y%m%d");

            next if Date_Cmp($date,$xdate) < 0;

            foreach my $sym(keys %duration) {
                next if ref $day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}};
                print "$date $sym " ,$day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}} ," \n " ;
                $sth->execute($date,$sym,$day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}});
            }
        }
    }
    else {
        my $day = $f->{LIST_G_NEW_DATE}{G_NEW_DATE};
        my $date=UnixDate(ParseDate($day->{BID_CURVE_DATE}),"%Y%m%d");

        next if Date_Cmp($date,$xdate) < 0;

        foreach my $sym(keys %duration) {
            next if ref $day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}};
            print "$date $sym " ,$day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}} ," \n " ;
            $sth->execute($date,$sym,$day->{LIST_G_BC_CAT}{G_BC_CAT}{$duration{$sym}});
        }
    }
}


END{ $dbh->disconnect if defined $dbh }
