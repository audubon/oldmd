#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  http://www.econstats.com
#   DATE:         3/26/05 10:44:40 AM
#---------------------------------------------------

use strict;
#use Data::Dumper;
use Date::Manip qw/Date_Cmp/;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use IO::Scalar;
$|++;

my $dbh=dbcnx();


# get the max date from the database and then only get the new prices
my $maxdtsql="select max(date) from metalspot where symbol=?";
my $maxsth = $dbh->prepare($maxdtsql);


my $insert = qq^
INSERT into metalspot
(date,symbol,close)
values
(?,?,?)
^;
my $sth = $dbh->prepare($insert);

my %metals = ( AL => 1
              ,CO => 2
              ,NI => 3
              ,LE => 4
              ,TI => 5
              ,ZI => 6
              ,AA => 7
              ,SI => 8
              ,NASAAC => 9  );

foreach my $symbol ( keys %metals ) {
    my $xx = $metals{$symbol};
    my $url= "http://www.econstats.com/fut/xlme__d$xx.htm";
    print $url , "\n";
    my $text=get($url);
        warn "could not get $url\n" unless defined $text;

    # compare with max(date) from table
    $maxsth->execute($symbol);
    my $maxdate=$maxsth->fetchrow_arrayref;


    my $sh = new IO::Scalar \$text;
    while ( defined(my $line = $sh->getline) ){
        next unless $line=~/^\d{6}\.\d{2}/;
        $line=~s/<[^>]*>//gs;
        my @data=split(',',$line);
        $data[1] =~s/\s//g;
        $data[0] =~s/\D//g;
        print "$symbol, $data[0] , $data[1] \n";

        last if Date_Cmp($data[0],$maxdate->[0]) < 0;

        $sth->execute($data[0],$symbol,$data[1]);
    }
    $sh->close;

}


END{ $dbh->disconnect if defined $dbh }
