#!/usr/bin/perl

#---------------------------------------------------
#   DESCRIPTION:  Get Big money inflows from IDB website and put in database
#   DATE:         2/15/05 7:33:38 PM
#   $Id: ibd.pl,v 1.2 2005/03/22 01:16:03 sdfjh Exp $
#
#---------------------------------------------------

use strict;
use PRC::DB qw/dbcnx/;
use LWP::Simple;
use HTML::TableExtract;
$|++;


my $url  = 'http://www.investors.com';
my $html = get($url);
die "Cannot get $url" unless defined $html;


my $dbh=dbcnx();

my $insert = qq^
INSERT INTO ibdbigmoney
(symbol,price,pricechg,volumechg,direction)
values
(?,?,?,?,'bull')
^;
my $sth = $dbh->prepare($insert);


my $te = new HTML::TableExtract( depth => 2, count => 7   );
#my $te = new HTML::TableExtract( headers => [ ('Stock Symbol','Price*','Price Chg','Vol % Chg') ]);
$te->parse($html);
foreach my $ts ($te->table_states) {
    foreach my $row ($ts->rows) {
        map{s/\s+//g}@$row;
        next unless $row->[0]=~/\w{2,5}/ && $row->[2]=~/\d+\.\d+/;
        map{s/[+,]//g} ($row->[4] , $row->[6]);
        print qq^$row->[0],$row->[2], $row->[4], $row->[6] \n^;
        $sth->execute($row->[0],$row->[2], $row->[4], $row->[6]);
    }
}
